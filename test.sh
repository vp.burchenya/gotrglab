cat <<EOF | http -v POST :3000/api/v1/
{
  "ip": "127.0.0.1",
  "name": "localhost",
  "clusters": [
    {
      "lab_cluster": [
        {
          "project_name": "lab01",
          "status": false
        },
        {
          "project_name": "lab02",
          "status": false
        }
      ]
    },{
      "local_cluster": [
        {
          "project_name": "loc01",
          "status": false
        },
        {
          "project_name": "loc02",
          "status": false
        }
      ]
    }
  ]
}
EOF

