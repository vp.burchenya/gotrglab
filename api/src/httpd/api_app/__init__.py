from .. import ( MongoApp, Request, Response, json_response, HTTPUnsupportedMediaType, HTTPBadRequest, HTTPNotFound )
from ..model import Server, Project, ValidationError, pprint as pp
import logging
from bson.objectid import ObjectId


__all__ = ['app']


app = MongoApp(logging.getLogger('httpd.api'))


@app.route(verb='GET', path='/')
async def findAll(req: Request):
    """
    ---
    description: List all server documents.
    tags:
    - crud
    produces:
    - application/json
    responses:
        "200":
            description: successful
    """
    cursor = app.collections.doc.find()
    docs = await cursor.to_list(length=128)
    servers = []
    while docs:
        servers += docs
        docs = await cursor.to_list(length=128)
    return json_response(Server(many=True).dump(servers))


@app.route(verb='POST', path='/')
async def create(req: Request):
    """
    ---
    description: Create server document.
    tags:
    - crud
    responses:
        "201":
            description: successful
        "400":
            description: on body validation errors
    """
    try:
        server = Server().load(req['document'])
        res = await app.collections.doc.insert_one(server)
        assert res.inserted_id
    except ValidationError:
        raise HTTPBadRequest
    
    return Response(status=201, text='201: Created')


@app.route(verb='POST', path=r'/{id:[\da-f]{24}}/{clu_name:[\d_A-Za-z]+}/')
async def create_project(req: Request):
    """
    ---
    description: Append project to server cluster.
    tags:
    - crud
    produces:
    - application/json
    responses:
        "201":
            description: successful append
        "400":
            description: on body validation errors
    """
    _id = ObjectId(req.match_info['id'])
    clu_name = req.match_info['clu_name']

    try:
        proj = Project().load(req['document'])
        doc = await app.collections.doc \
                    .find_one_and_update({ '_id': _id,
                                         f"clusters.{clu_name}" : {"$exists": True} },
                                         { '$push' : {f"clusters.$.{clu_name}" : proj} })
        if not doc:
            raise HTTPNotFound
   
    except ValidationError:
        raise HTTPBadRequest
    
    return Response(status=201, text='201: Created')
