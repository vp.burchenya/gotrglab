from . import HTTPd, Request, Response
from .api_app import app as api_app
from aiohttp_swagger import setup_swagger
import logging 


__all__ = ('app')

logging.basicConfig(format = "[%(name)s] %(message)s")
logging.root.setLevel(logging.INFO)
logging.getLogger('httpd').setLevel(logging.DEBUG)
logging.getLogger('aiohttp.access').setLevel(logging.INFO)
    
# loop = asyncio.get_event_loop()
app = HTTPd(logging.getLogger('httpd.app'))
app.add_subapp('/api/v1', api_app)

@app.route(path='/')
async def index(req: Request):
    return Response(status=200, text='index', content_type='text/plain')

# http://localhost:3000/api/doc
setup_swagger(app)
