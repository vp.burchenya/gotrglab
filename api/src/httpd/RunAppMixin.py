from aiohttp.web import run_app


__all__ = ('RunAppMixin')


class RunAppMixin(object):
    def start(self, port=8080):
        return run_app(self, port=port)
