from marshmallow import Schema, fields, ValidationError


__all__ = ('Project', 'Server')


class ClusterField(fields.Field):
    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self._name = fields.Str()
        self._projects = fields.Nested(Project(many=True))

    def _deserialize(self, val, attr, data):
        if not isinstance(val, dict) or not not len(val)-1:
            raise ValidationError('Not a valid object')

        return { self._name.deserialize(k) : self._projects.deserialize(v)
                 for (k, v) in val.items() }


class Project(Schema):
    project_name = fields.Str(required=True)
    status = fields.Bool(required=True)


class Server(Schema):
    _id = fields.Str(data_key='id', dump_only=True)
    ip = fields.Str(required=True)
    name = fields.Str(required=True)
    clusters = fields.List( ClusterField(), required=True )
