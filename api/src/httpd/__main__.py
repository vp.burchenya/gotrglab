def start():
    from .httpd_app import app
    app.start(3000)


if __name__ == '__main__':
    start()
