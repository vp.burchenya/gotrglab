from aiohttp.web \
import ( middleware,
         Application,
         Request,
         Response,
         json_response,
         HTTPError,
         HTTPBadRequest,
         HTTPUnsupportedMediaType,
         HTTPNotFound,
         HTTPNotAcceptable,
         HTTPServerError )
import logging, json
from .RunAppMixin import RunAppMixin
import motor.motor_asyncio as motor
import mimeparse


__all__ = ['HTTPd', 'HTTPApp', 'MongoApp', 'Request', 'Response']



@middleware
async def unmarshall(req: Request, handler) -> Response:
    if req.method in ('POST', 'PUT'):
        if not req.content_type == 'application/json':
            raise HTTPUnsupportedMediaType
        else:
            try:
                req['document'] = await req.json()
            except json.decoder.JSONDecodeError:
                raise HTTPBadRequest

    res = await handler(req)
    return res


@middleware
async def accepts(req: Request, handler) -> Response:
    accept = req.headers.get('Accept', '*/*')
    print(accept)
    res = await handler(req)

    if res.content_type and \
    not mimeparse.best_match([res.content_type], accept):
        raise HTTPNotAcceptable

    return res



class HTTPApp(Application):
    # todo: mimic app.use(middleware)
    def __init__(self, log: logging.Logger, middlewares=[]):
        Application.__init__(self, middlewares=middlewares)
        self.MAX_UPLOAD_SIZE = 1024  # 1024**2 = 1M
        self.log = log
    
    def subroute(self, prefix='/'):
        def _(verb='GET', path='/'):
            return self.route(verb, prefix.rstrip('/') +
                                    '/' +
                                    path.strip('/'))
        return _

    def route(self, verb='GET', path='/'):
        def decorator(fn):
            async def w(req: Request) -> Response:
                self.log.debug(f"Handle request: {verb} '{req.path}'")

                try:
                    r = await fn(req)
                except Exception as ex:
                    if isinstance(ex, HTTPError):
                        raise ex
                    else:
                        self.log.error(str(ex))
                        return Response(status=500, text='500: Internal Server Error')
                return r
            self.router.add_route(verb, path, w)
            return w
        return decorator


async def mongo_ctx_provider(app):
    # hardcoded uri
    url = "mongodb://localhost:27017"
    con = motor.AsyncIOMotorClient(url,
                                   maxPoolSize=2,
                                   io_loop=app.loop)
    app['db'] = con['gotrglab']

    await app.collections.doc.drop()
    await app.collections.doc.insert_one({
        "clusters": [
            {
                "debug_cluster": [
                    {
                        "project_name": "project_1",
                        "status": True
                    }
                ]
            }
        ],
        "ip": "11.12.13.14",
        "name": "debug_server"
    })

    yield
    con.close()


class MongoApp(HTTPApp):
    def __init__(self, log):
        super().__init__(log)
        self.cleanup_ctx.append(mongo_ctx_provider)
        self.collections = self.__class__.Coollections(self)

    class Coollections(object):
        def __init__(self, parent):
            self.parent = parent
        def __getattr__(self, name):
            return self.parent.database[name]

    @property
    def database(self):
        return self['db']


class HTTPd(HTTPApp, RunAppMixin):
    def __init__(self, log):
        super().__init__(log, middlewares=[ unmarshall, accepts ])
