#!/usr/bin/env /bin/bash
CURRENT_DIR="$(dirname "$(readlink -f "$0")")"
env -u PYTHONHOME PATH="${CURRENT_DIR}/venv/bin/:${PATH}" PYTHONPATH="${CURRENT_DIR}/src/" python -m httpd

