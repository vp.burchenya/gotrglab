#!/bin/bash
CTNAME="gotrglab-db"
docker run \
  -d \
  --rm \
  -ti \
  --name "${CTNAME}" \
  -h "${CTNAME}" \
  -p 27017:27017 \
  -v "$(dirname "$(readlink -f "$0")")/db/":/data/db/ \
  mongo
