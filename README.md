## Тестовое задание
##### Test
- Use Linux !
- Use virtualenv ?
- pip install -r requirements
- Start MongoDB on localhost for anonymous, see: `/api/mongodb.sh`
- Start API, see `/api/start.sh`
- Start WWW in dev: `npm run httpd:dev`

##### Todo
- Docker compose
- CORS или Nginx forward
- OpenAPI