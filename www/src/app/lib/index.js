'use strict'
import 'babel-polyfill'  // IE11 & Safari9
import 'material-design-icons-iconfont/dist/material-design-icons.css'  // vs vue-material-design-icons
// import '@mdi/font/css/materialdesignicons.css'  // vs @mdi/font
import 'vuetify/dist/vuetify.css'
import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import App from '@C/App/index.vue'
import Axios from 'axios'


Vue.config.devtools = true
Vue.config.debug = true

Vue.use(Vuetify)
Vue.use(VueRouter)

Vue.prototype.$ua = Axios

Vue.component('v-hello', {
    render: h => h('div', {}, [ h('p', 'Home') ])
})

Object.defineProperty(Vue.prototype, "$bus", {
    get: function() {
        return this.$root.bus
    }
})


const vm = new Vue({

    render: h => h(App),
    data: () => ({
        bus: new Vue({})
    }),
    components: {},
    mounted: function() {
        // top mounted last
        self = this
        this.$nextTick(() => {
            // todo: remove listeners
            window.addEventListener('resize', function(_ev) {
                self.bus.$emit('dom:resize')
            })
        })
    }

}).$mount('#app')

// document.getElementById("app").__vue__
window.vm = vm
