'use strict'
import template from './index.t.html'


export default {
    name: 'Example',
    template: template,
    props: {
        msg: String
    }
}
