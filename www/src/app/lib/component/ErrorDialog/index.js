'use strict'


export default {
    name: 'ErrorDialog',
    props: [ 'message' ],
    computed: {
        display: function () { return !!this.message }
    }
}
