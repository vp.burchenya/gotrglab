'use strict'
import ServerList from '@C/ServerList/index.vue'
import ClusterList from '@C/ClusterList/index.vue'
import ProjectList from '@C/ProjectList/index.vue'
import ErrorDialog from '@C/ErrorDialog/index.vue'
import VueRouter from 'vue-router'


export default {
    name: 'App',
    props: {},
    data: () => ({
        servers: [],
        error: ''
    }),
    components: {
        [`X${ErrorDialog.name}`]: ErrorDialog
    },
    router: new VueRouter({
        routes: [
            { path: '/', name: 'index', component: ServerList, props: {}},
            { path: '/:ix/', name: 'clusters', component: ClusterList, props: true },
            { path: '/:ix/:cluster/', name: 'projects', component: ProjectList, props: true }
        ]
    }),
    mounted: async function() {

        let self = this  // apply/bind

        async function reload () {
            try {
                var res = await self.$ua.get('/api/v1/')
                self.servers = res.data
            } catch(e) {
                // throw e
                let code = e.response && e.response.status || 'system'
                self.$bus.$emit('error', new Error(code))
            }
        }

        this.$bus.$on('error', (err) => {
            console.error(`${err.message} error`)
            this.error = err.message
        })
        this.$bus.$on('rescue', () => this.error = '')
        this.$bus.$on('reload', reload)
        this.$bus.$on('createpj', this.on_create)
        this.$bus.$emit('reload')
    },
    watch: {
        '$route' (to, from) {}
    },
    methods: {
        async on_create ({name, status, id, cluster} = o) {
            try {
                await self.$ua.post(`/api/v1/${id}/${cluster}/`,
                                    { project_name: name, status })
                this.$bus.$emit('reload')
            } catch(e) {
                let code = e.response && e.response.status || 'system'
                self.$bus.$emit('error', new Error(code))
            }
        }
    }
}
