'use strict'


export default {
    name: 'CreateDialog',
    props: [ 'id', 'cluster' ],
    data: () => ({
        display: false,
        name: '',
        status: false
    }),
    methods: {
        create () {
            this.$bus.$emit('createpj', { name: this.name,
                                          status: this.status,
                                          id: this.id,
                                          cluster: this.cluster })
            this.display = false
            this.name = ''
        }
    }
}
