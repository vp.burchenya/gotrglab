'use strict'


export default {
    name: 'ClusterList',
    props: ['servers', 'ix'],
    data: () => ({
        // hostname: ''
    }),
    computed: {
        hostname: function () {
            let server = this.servers[this.ix]
            return server ? server.name : undefined
        },
        clusters: function () {
            //this.$route.params
            let server = this.servers[this.ix]

            let { clusters=[], ..._ } = server || {}

            let res = []
            for (let clu of clusters) {
                for (let clu_name of Object.keys(clu)) {
                    res.push(clu_name)
                    //yield clu_name  // vue not watch
                }
            }
            return res
        }
    }
}
