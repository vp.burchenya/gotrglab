'use strict'
import CreateDialog from '@C/CreateDialog/index.vue'


export default {
    name: 'ProjectList',
    props: ['servers', 'ix', 'cluster'],
    data: () => ({
        // hostname: '',
        // id: ''
    }),
    mounted: function() {
        this.$on('createpj', () => alert('xxx'))
    },
    components: {
        [`X${CreateDialog.name}`]: CreateDialog
    },
    computed: {
        id: function () {
            let server = this.servers[this.ix]
            return server ? server.id : undefined
        },
        hostname: function () {
            let server = this.servers[this.ix]
            return server ? server.name : undefined
        },
        projects: function () {
            let server = this.servers[this.ix]
            let { clusters=[], ..._rest } = server || {}
            let res = []

            for (let clu of clusters) {
                for (let clu_name of Object.keys(clu)) {
                    if (clu_name == this.cluster)
                        for (let pj of clu[clu_name]) {
                            let { project_name: name, status: status } = pj
                            res.push({ name, status })
                        }
                }
            }
            return res
        }
    }
}
