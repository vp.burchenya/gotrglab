'use strict'


const webpack = require('webpack')
const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')


module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: {
        'index': './src/app/lib/index',
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        port: 9090,
        proxy: {
            '/api/v1': {
                 target: {
                     host: '0.0.0.0',
                     protocol: 'http:',
                     port: 3000
                 },
                 pathRewrite: {}
            }
        }
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'lib/[name].js',
        // publicPath: '/'
    },
    resolve: {
        alias: {
            // vue: 'vue/dist/vue.min.js', // by default esm runtime does not include template compiler
            'vue$': 'vue/dist/vue.esm.js',
            '@': path.join(__dirname, '.', '.'),
            '@C': path.join(__dirname, './src/app/lib/component')
        }
    },
    plugins: [
        // new DefinePlugin()
        new CopyWebpackPlugin([
            { from: './src/*.html', to: './[name].[ext]' },
            { from: './src/favicon.ico' },
        ]),
        new webpack.ProvidePlugin({
            _: 'lodash',
            _map: ['lodash', 'map'],
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            // _vue: ['vue/dist/vue.esm.js', 'default']  // default es2015 export
        }),
        new VueLoaderPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [{
                    loader: 'vue-loader'
                }]
            },
            {
                test: /\.(txt|t.html)$/,
                use: [{
                    loader: 'raw-loader'
                }]
            },
            // {
            //     test: /\.css$/,
            //     use: ExtractTextPlugin.extract({
            //         fallback: 'style-loader',
            //         use: 'css-loader'
            //     })
            // },
            {
                test: /\.js$/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: [['@babel/preset-env', {
                            "modules": false,
                            "targets": {
                                "browsers": ["> 1%", "last 2 versions", "not ie <= 8"]
                            }
                        }]],
                        plugins: ['@babel/plugin-syntax-dynamic-import']
                    }
                }],
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpeg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[hash:8].[ext]',
                        outputPath: 'assets/',
                        publicPath: '/img'
                    }
                }]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
                // test: /.*\.(eot|woff2?|ttf|svg)(\?(#\w+&)?v=\d+\.\d+\.\d+(#\w+)?)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'font/[name].[hash:8].[ext]'
                        // outputPath: 'fonts/',
                    }
                }]
            },
            {
                test: /\.css$/,
                oneOf: [
                  {
                    resourceQuery: /module/,
                    use: [
                      'vue-style-loader',
                      {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            localIdentName: '[local]_[hash:base64:8]'
                        }
                      }
                    ]
                  },
                  {
                    use: [
                      'vue-style-loader',
                      'css-loader'
                    ]
                  }
                ]
            }
        ]
    }
}

