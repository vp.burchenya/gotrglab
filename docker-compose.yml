---
version: "3.6"  # 3.7 is last


x-web-domainname: &web-domainname oz.net.ru

x-default-service:
  &default-service
  # docker swarm internal dns server: `127.0.0.11
  # domainname: *domainname
  dns_search: []

x-default-restart-policy:
  &default-restart-policy
  condition: on-failure
  delay: 60s
  max_attempts: 3
  window: 120s

x-default-deploy:
  &default-deploy
  mode: replicated
  replicas: 1
  restart_policy: *default-restart-policy
  placement:
    constraints:
      - node.role == manager
      - node.hostname != prod

x-hostname-frmt: &hostname-frmt "{{.Node.Hostname}}_{{.Service.Name}}_{{.Task.ID}}"

services:
  api:
    <<: *default-service
    depends_on: ["db"]
    container_name: "gotrglab-api"
    hostname: *hostname-frmt
    image: oz-net/gotrglab-db-api:latest
    build: ./api/
    ports:
      - "8081:3000/tcp"
    networks:
      gotrglab-net:
        aliases:
          - ws
    volumes:
      - shared:/mnt/shared
    healthcheck:
      test: >-
        python3.7 -c "from urllib.request import urlopen;
        assert 200 == urlopen('http://127.0.0.1:3000/health').status,
        'failed state'"
      interval: 1m00s
      timeout: 10s
      retries: 1
      start_period: 10s
    deploy:
      <<: *default-deploy
      labels:
        ru.net.oz.app: gotrglab-api

  www:
    <<: *default-service
    depends_on: ["api", "html"]
    container_name: "gotrglab-www"
    hostname: *hostname-frmt
    image: oz-net/gotrglab-www:latest
    build: ./www/
    ports:
      - '8082:80/tcp'
    networks:
      - gotrglab-net
    volumes:
      - shared:/mnt/shared
    # volumes_from: todo
    deploy:
      <<: *default-deploy
      labels:
        ru.net.oz.app: gotrglab-www

  html:
    image: oz-net/gotrglab-html:latest
    build:
      context: ./www/
      dockerfile: Dockerfile
    tty: true
    stdin_open: true
    command: ["true"]

  lb:
    <<: *default-service
    depends_on: ["api", "www"]
    container_name: "gotrglab-lb"
    hostname: *hostname-frmt
    image: oz-net/gotrglab-lb:latest
    build: ./lb/
    environment:
      WEB_DOMAIN: *web-domainname
    ports:
      - '80:80/tcp'
    networks:
      - gotrglab-net
    deploy:
      <<: *default-deploy
      labels:
        ru.net.oz.app: gotrglab-lb

  db:
    <<: *default-service
    container_name: "gotrglab-db"
    hostname: *hostname-frmt
    image: mongo
    # build: no
    environment:
      MONGO_HOST: 'localhost'
    ports:
      - '5432:5432/tcp'
    networks:
      gotrglab-net:
        aliases:
          - nosql
    # volumes:
    #   - database:/var/lib/db/

    deploy:
      <<: *default-deploy
      labels:
        ru.net.oz.app: gotrglab-db

  meta:
    container_name: "gotrglab-meta"
    hostname: *hostname-frmt
    image: debian:buster-slim
    networks:
      gotrglab-net:
        ipv4_address: 169.254.169.254
    deploy:
      <<: *default-deploy
      labels:
        ru.net.oz.app: gotrglab-meta
    command: cat

  # visualizer:
  #   image: dockersamples/visualizer:stable
  #   ports:
  #     - "8090:8080"
  #   stop_grace_period: 1m00s
  #   volumes:
  #     - "/var/run/docker.sock:/var/run/docker.sock"

  portainer:
    image: portainer/portainer
    ports:
      - '9000:9000'
    volumes:
      - ./portainer_data:/data
      - /var/run/docker.sock:/var/run/docker.sock
    deploy:
      mode: global  # on each node
      placement:
        constraints:
          - node.role == manager


networks:
  gotrglab-net:
  test-net:
    driver: overlay
    attachable: true
    ipam:
      config:
        - subnet: 192.168.0.0/24
          # aux_addresses:
          #   meta: 192.168.0.254

volumes:
  shared:
  database:
